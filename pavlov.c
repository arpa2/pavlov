/** pavlov.c -- Wrapper program around libpavlov.
 *
 * This is a substitute for chat(8) functionality for expect/response
 * interactions, founded on extended regular expressions (after ?) and
 * string printing (after !) as well as milli-second delays (after @).
 *
 * If you invoke this program as pavlov-server or pavlov-client, it
 * expects IP and PORT args, and it will serve or connect over TCP/IP.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <libgen.h>
#include <errno.h>

#include <getopt.h>

#include <arpa2/socket.h>

#include "pavlov.h"


int main (int argc, char *argv []) {
	bool ok = true;
	bool got_socket = false;
	//
	// Parse commandline
	char *progname = basename (argv [0]);
	bool once = 0;
	char *pypeliner = NULL;
	char *client_port = NULL;
	char *server_port = NULL;
	char *ip_address  = NULL;
	struct option longopts [] = {
		{ "help",	no_argument,       0, 'h' },
		{ "ip",		required_argument, 0, 'i' },
		{ "client",	required_argument, 0, 'c' },
		{ "server",	required_argument, 0, 's' },
		{ "pypeliner",	optional_argument, 0, 'p' },
		{ "once",       no_argument,       0, 'o' },
		{ NULL, 0, NULL, 0 }
	};
	int opt;
	while (opt = getopt (argc, argv, "hoi:c:s:p::"), opt != -1) {
		switch (opt) {
		case 'i':
			/* -i for an IPv6 or IPv4 address */
			ip_address = optarg;
			break;
		case 'c':
			/* -c for a client port */
			client_port = optarg;
			break;
		case 's':
			/* -s for a server port */
			server_port = optarg;
			break;
		case 'p':
			/* -p for pypeline output */
			pypeliner = (optarg != NULL) ? optarg : "--";
			break;
		case 'o':
			/* -o to run once (for a server) */
			once = true;
			break;
		case 'h':
		default:
			fprintf (stderr, "Usage: %s [-i|--ip ADDRESS] [-c|-s|--client|--server PORT] [-p|--pypeliner [LINE]] ...\n", progname);
			exit ( (opt == 'h') ? 0 : 1 );
		}
	}
	argc -= optind;
	argv += optind;
	bool client = (client_port != NULL);
	bool server = (server_port != NULL);
	if (client && server) {
		fprintf (stderr, "You cannot choose both client and server mode\n");
		exit (1);
	}
	if (ip_address == NULL) {
		ip_address = "::1";
	} else if ((!client) && (!server)) {
		fprintf (stderr, "If you specify an address, you should also set a client or server port\n");
		exit (1);
	}
	//
	// Setup fdin, fdout -- possible from a socket
	int fdin  = -1;
	int fdout = -1;
	if (client || server) {
		struct sockaddr_storage sa;
		int sox = -1;
		got_socket = true;
		socket_init ();
		ok = ok && socket_parse (ip_address, client ? client_port : server_port, &sa);
		ok = ok && (server ? socket_server : socket_client) (&sa, SOCK_STREAM, &sox);
		if (ok && (pypeliner != NULL)) {
			printf ("%s\n", pypeliner);
			fflush (stdout);
		}
		if (server) {
			while (ok) {
				int cnx = accept (sox, NULL, 0);
				if ((cnx < 0) && (errno != EINTR)) {
					continue;
				}
				if (once) {
					/* one-shot */
					close (sox);
					sox = cnx;
					break;
				}
				pid_t pid = fork ();
				ok = ok && (pid >= 0);
				if (pid == 0) {
					/* slave */
					close (sox);
					sox = cnx;
					break;
				}
				if (pid > 0) {
					/* server */
					close (cnx);
					continue;
				}
			}
		}
		if (!ok) {
			perror ("Failed to setup the network");
			exit (1);
		}
		fdin = fdout = sox;
	} else {
		fdin = 0;
		fdout = 1;
	}
#ifdef SMTP_SPECIFICS
	//
	// Is there no argument?  Then try the default sequence
	//TODO// Search for "proto", smtp-client etc.
	char *proto = strchr (progname, '-');
	if ((argc == 0) && server) {
		argv = smtp_server;
		argc = sizeof (smtp_server) / sizeof (char *);
	}
	if ((argc == 0) && client) {
		argv = smtp_client;
		argc = sizeof (smtp_client) / sizeof (char *);
	}
#endif
	//
	// Pavlov operates on the final values of argc, argv
	int argerr = ok ? pavlov (fdin, fdout, progname, argc, argv) : 1;
	//
	// If we setup a socket, close it now
	if (got_socket) {
		if (fdin >= 0) {
			socket_close (fdin);
		}
		fdin = fdout = -1;
		socket_fini ();
	}
	//
	// Exit now, possibly with an error code
	exit ( (argerr > 0) ? 1 : 0 );
}
