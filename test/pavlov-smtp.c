/* SMTP client and server using Pavlov.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <signal.h>
#include <libgen.h>
#include <errno.h>

#include <arpa2/socket.h>

#include "../pavlov.h"


char *smtp_client [] = {
	"?^220 .*$",
	"!EHLO orvelte.nep\r\n",
	"?250[- ]STARTTLS\r\n",
	"!STARTTLS\r\n",
	"?^220 .*$",
};

char *smtp_server [] = {
	"!220 devnull.example.com ESMTP Pavlov\r\n",
	"?^EHLO .+$",
	"!250-devnull.example.com ready to loose your message\r\n"
	"250-SIZE 13\r\n"
	"250-8BITMIME\r\n"
	"250-STARTTLS\r\n"
	"250-ENHANCEDSTATUSCODES\r\n"
	"250 SMTPUTF8\r\n",
	"?^STARTTLS\r\n",
	"!220 2.0.0 Ready to start TLS\r\n",
};


void interrupt (int sig) {
	exit (0);
}


int main (int argc, char *argv []) {
	bool ok = true;
	bool got_socket = false;
	//
	// Parse commandline -- really easily
	char *progname = basename (argv [0]);
	argc--;
	argv++;
	//
	// Setup interrupt handler
	signal (SIGINT, interrupt);
	//
	// Setup fdin, fdout -- possibly from a socket
	int fdin  = -1;
	int fdout = -1;
	char *progside = strrchr (progname, '-');
	bool client = (progside != NULL) && (0 == strcmp (progside, "-client"));
	bool server = (progside != NULL) && (0 == strcmp (progside, "-server"));
	if (client || server) {
		struct sockaddr_storage sa;
		int sox = -1;
		if (argc < 2) {
			fprintf (stderr, "Usage: %s IP PORT [expr...]\n", progname);
			exit (1);
		}
		got_socket = true;
		socket_init ();
		ok = ok && socket_parse (argv [0], argv [1], &sa);
		argc -= 2;
		argv += 2;
		if (server) {
			ok = ok && socket_server (&sa, SOCK_STREAM, &sox);
			puts ("--");
			fflush (stdout);
			while (ok) {
				int cnx = accept (sox, NULL, 0);
				if ((cnx < 0) && (errno != EINTR)) {
					continue;
				}
				pid_t pid = fork ();
				ok = ok && (pid >= 0);
				if (pid == 0) {
					/* slave */
					close (sox);
					sox = cnx;
					break;
				}
				if (pid > 0) {
					/* server */
					close (cnx);
					continue;
				}
			}
		} else {
			ok = ok && socket_client (&sa, SOCK_STREAM, &sox);
			puts ("--");
			fflush (stdout);
		}
		if (!ok) {
			perror ("Failed to setup the network");
			exit (1);
		}
		fdin = fdout = sox;
	} else {
		fdin = 0;
		fdout = 1;
	}
	//
	// Is there no argument?  Then try the default sequence
	//TODO// Search for "proto", smtp-client etc.
	char *proto = strchr (progname, '-');
	if ((argc == 0) && server) {
		argv = smtp_server;
		argc = sizeof (smtp_server) / sizeof (char *);
	}
	if ((argc == 0) && client) {
		argv = smtp_client;
		argc = sizeof (smtp_client) / sizeof (char *);
	}
	//
	// Pavlov operates on the final values of argc, argv
	int argerr = ok ? pavlov (fdin, fdout, progname, argc, argv) : 1;
	//
	// If we setup a socket, close it now
	if (got_socket) {
		if (fdin >= 0) {
			socket_close (fdin);
		}
		fdin = fdout = -1;
		socket_fini ();
	}
	//
	// Exit now, possibly with an error code
	exit ( (argerr > 0) ? 1 : 0 );
}
