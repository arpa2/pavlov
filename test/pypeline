#!/usr/bin/env python3
#
# pypeline builds a pipeline of network units, and keeps an eye on
# their successful termination.  It allows quick testing setups with
# an input stage in Python, an intermediate program run in a (portable)
# subprocess, and finally an output stage in Python.  The Python units
# run as threads and are usually called with a set of arguments, in
# the style of argv[1:].
#
# Specify commands and arguments on the pypeline command line; "--"
# separates the various commands.  They are started in turn, and
# each must output "--\n" after it has finished completion (and is
# ready for interaction).  Furthermore, there may be port numbers,
# allocated dynamically when they are specified as "TCP:<key>" or
# "UDP:<key>" or "SCTP:<key>" where the "<key>" is a string to
# identify the same port (which may be empty).  A fresh port is then
# allocated from the ephemeral space, and its number is passed to
# the subprocesses as a commandline argument in that position.  In
# a similar fashion, there can be "IP:<key>" to specify the local
# host as an IP address.
#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2020, Rick van Rein <rick@openfortress.nl>


import os
import sys
import time
import re

import socket
import signal
import threading
import subprocess 
import tempfile
# import optparse


if sys.argv [1] == '--' or sys.argv [-1] == '--':
	sys.stderr.write ('Usage: %s cmd1 arg1a arg1b -- cmd2 arg2a -- cmd3 arg3a arg3b\n' % (sys.argv [0],))
	sys.exit (1)


# Introduce colours when they are available
#
# To know which ones _you_ want to remove:
#
# import colored
# for c in colored.names:
#	c = c.lower ()
#	print ('%s%s  ' % (colored.fg (c),c), end='')
#
try:
	from colored import fg, bg, attr, colors
	colours = [ cn.lower () for cn in colors.names ]
	colours.remove ('white')
	colours.remove ('black')
	colours.remove ('grey_0')
	colours.remove ('grey_100')
	colours.remove ('blue')
	colours.remove ('dark_gray')
	colours.remove ('blue_3a')
	colours.remove ('blue_3b')
	colours.remove ('dark_red_1')
	colours.remove ('deep_pink_4a')
	colours.remove ('purple_4a')
	colours.remove ('grey_19')
	colours.remove ('grey_23')
	colours.remove ('grey_27')
	colours.remove ('grey_30')
	colours.remove ('grey_35')
except:
	def attr (_col):
		return ''
	fg = attr
	bg = attr
	colours = [ 'BLASS' ]

colour_idx = 0
def pick_colour ():
	global colours, colour_idx
	colour = colours [colour_idx % len (colours)]
	colour_idx += 1
	return colour


# The prox and zigs lists collect the arguments in [0] and any
# new command is inserted in front, to assure handling from the
# last program on the commandline to the earlier ones.  This is
# the order in which programs are started.
#
prox = [ [] ]
pidz = [ [] ]
zigz = [ [] ]
exok = [ [] ]
fork = [ '--' ]
namz = [ None ]

sigtypes = int (signal.Signals.SIGHUP)

tmpdir = None

env = dict (os.environ)

try:
	sox6 = socket.socket (socket.AF_INET6)
	sox6.bind ( ('::1',0) )
	myip = sox6.getsockname () [0]
	sox6.close ()
	myfam = socket.AF_INET6
except:
	sox4 = socket.socket (socket.AF_INET)
	sox4.bind ( ('127.0.0.1',0) )
	myip = sox4.getsockname () [0]
	sox4.close ()
	myfam = socket.AF_INET

soxtypes = {
	'TCP:':  (myfam, socket.SOCK_STREAM,    0),
	'UDP:':  (myfam, socket.SOCK_DGRAM,     0),
	'SCTP:': (myfam, socket.SOCK_SEQPACKET, 0),
}

port_alloc = { }

keyed_random = { }

intlist_re = re.compile ('^[0-9]+(,[0-9]+)*$')

argvcp_files = { }
argvcp_map = { '': '#' }

for argi in sys.argv [1:]:
	precmd = ( prox [0] == [] )
	proto = argi [:1+argi.find (':')]
	argo = None
	if argi == '--':
		# Push the stack; insert new items at index 0
		prox.insert ( 0,[] )
		pidz.insert ( 0,[] )
		zigz.insert ( 0,[] )
		exok.insert ( 0,[] )
		fork.insert ( 0,'--' )
		namz.insert ( 0,None )
	elif proto == 'FILE:':
		if tmpdir is None:
			tmpdir = tempfile.mkdtemp ()
		argo = tmpdir + os.sep + argi [5:] + '.pypefile'
	elif proto == 'PIDFILE:':
		if tmpdir is None:
			tmpdir = tempfile.mkdtemp ()
		pidz [0].append (tmpdir + os.sep + argi [8:] + '.pypefile')
		# Drop the 'PID'
		argi = argi [3:]
		argo = pidz [0][-1]
	elif proto == 'INFILE:':
		if tmpdir is None:
			tmpdir = tempfile.mkdtemp ()
		(shared,input) = argi [7:].split (':', 1)
		# Drop the 'IN' and the original file
		argi = 'FILE:' + shared
		argo = tmpdir + os.sep + shared + '.pypefile'
		argvcp_files [argo] = input
	elif proto == 'SIG:':
		signame = argi [:3] + argi [4:]
		try:
			signum = int (getattr (signal.Signals, signame, None))
		except:
			sys.stderr.write ('No signal named %s is known' % (signame,))
			sys.exit (1)
		zigz [0].append ( (signame,signum) )
		# SIG:xxx before a daemon specifies its intrinsic takedown signal
		argo = str (signum)
	elif proto == 'IP:':
		argo = myip
	elif proto == '[IP]:':
		if myfam == socket.AF_INET:
			argo = myip
		else:
			argo = '[%s]' % myip
	elif proto == 'RNDHEX:':
		key = argi [7:]
		if not key in keyed_random:
			import random
			prng = random.Random ()
			rnd = ''.join (prng.choices ('0123456789abcdef', k=32))
			keyed_random [key] = rnd
		argo = keyed_random [key]
	elif proto == 'PYPE:':
		if argi [:10] == 'PYPE:FORK:':
			if precmd:
				fork [0] = argi [10:]
			else:
				# Append the string to the command, may be used to add -- as argument
				argo = argi [10:]
		elif argi [:10] == 'PYPE:EXIT:':
			exok [0].append (int (argi [10:]))
		elif argi [:12] == 'PYPE:POPCAT:':
			try:
				popcount = int (argi [12:])
				assert 2 <= popcount <= len (prox [0])
			except:
				sys.stderr.write ('Invalid integer argument in %s\n', argi)
				raise
			prox [0] = prox [0] [:-popcount] + [ ''.join (prox [0] [-popcount:]) ]
		elif argi [:10] == 'PYPE:NAME:':
			if len (argi) > 10:
				argo = argi [10:]
			elif namz [0] is not None:
				argo = namz [0]
			else:
				sys.stderr.write ('First use of PYPE:NAME: must be followed by a name\n')
				sys.exit (1)
			namz [0] = argo
		elif argi [:9] == 'PYPE:ESC:':
			argo = argi [9:]
		else:
			sys.stderr.write ('Invalid Pypeline control sequence %s\n' % (argi,))
			sys.exit (1)
	elif proto == 'ENV:':
		envvar = argi [4:]
		validx = envvar.find ('=')
		if validx < 0:
			# Variable name only, require existence
			if not envvar in env:
				sys.stderr.write ('Missing environment variable %s\n' % (envvar,))
				sys.exit (1)
		else:
			# Variable=Value
			envval = envvar [validx+1:]
			envvar = envvar [:validx]
			env [envvar] = envval
		argo = env [envvar]
	elif proto in soxtypes:
		if not argi in port_alloc:
			sox = socket.socket ( *soxtypes [proto] )
			sox.bind ( (myip, 0) )
			port = str (sox.getsockname () [1])
			sox.close ()
			port_alloc [argi] = port
		argo = port_alloc [argi]
	else:
		prox [0].append (argi)
	if argo is not None:
		argvcp_map [argi] = argo
		if not precmd:
			prox [0].append (argo)


#
# Now apply the argvcp_map to all argvcp_files
#
argvcp_bad = False
for (shared,original) in argvcp_files.items ():
	inf = open (original, 'r')
	otf = open (shared,   'w')
	for ln in inf.readlines ():
		for (it,pc) in enumerate (ln.split ('#')):
			if it & 0x01 == 0x00:
				otf.write (pc)
			elif pc in argvcp_map:
				otf.write (argvcp_map [pc])
			elif pc [:4] == 'ENV:' and pc [4:] in env:
				otf.write (env [pc [4:]])
			else:
				sys.stderr.write ('Unrecognised pattern #%s# in %s\n' % (pc,original))
				argvcp_bad = True
	inf.close ()
	otf.close ()
	if argvcp_bad:
		sys.exit (1)


phase1_mutex = threading.Condition ()
phase1_threads = set ()

def binline_as_text (bln):
	try:
		return str (bln, 'utf-8')
	except Exception as e:
		# hexes = ' '.join (['%02x' % bch for bch in bln])
		# return ('EXCEPTION %s IN %s\n' % (e,hexes))
		return 'EXCEPTION %s IN %r\n' % (e,bln)

class ErrorTapper (threading.Thread):

	def __init__ (self, stderr, addfun):
		threading.Thread.__init__ (self)
		self.stderr = stderr
		self.addfun = addfun

	def run (self):
		nextln = self.stderr.readline ()
		while nextln != b'':
			nextln = binline_as_text (nextln)
			self.addfun (nextln)
			nextln = self.stderr.readline ()


class SyncedProcess (threading.Thread):

	def __init__ (self, argv, name=None, signals=[], ok_exit=set ([0]), fork='--', env={}, pidfiles=[]):
		threading.Thread.__init__ (self, name=(name or argv [0]))
		self.error = None
		self.signals = signals
		self.pidfiles = pidfiles
		self.ok_exit = ok_exit
		self.colour = pick_colour ()
		self.proc = subprocess.Popen (
						argv,
						env=env,
						stdin=None,
						stdout=subprocess.PIPE,
						stderr=subprocess.PIPE)
						#IMPLIES:TEXT# universal_newlines=True)
		self.sofar = ''
		self.sobad = ''
		self.ertap = ErrorTapper (self.proc.stderr, self.errorline)
		self.ertap.start ()
		# '^' means fork now, '$' means "fork" at end, otherwise append '\n' and wait
		if fork != '^':
			if fork == '$':
				fork = ''
			else:
				fork = fork + '\n'
			self.copyloop (fork)
		self.phase1locker = len (signals) == 0
		if self.phase1locker:
			phase1_mutex.acquire ()
			phase1_threads.add (self)
			phase1_mutex.release ()

	def copyloop (self, terminator):
		nextln = None
		while nextln != terminator:
			nextln = self.proc.stdout.readline ()
			nextln = binline_as_text (nextln)
			if nextln != terminator:
				if nextln == '':
					sys.stderr.write ('Program %s output ended before marker "%s"\n' % (self.name, terminator.strip (),))
					self.error = 1
					return
				attrs = attr ('dim') if 'DEBUG' in nextln else ''
				attrs += fg (self.colour)
				sys.stdout.write (attrs + '%s: %s' % (self.name, nextln) + attr (0))
			self.sofar += nextln

	def errorline (self, nextln):
		attrs = attr ('bold') if not 'DEBUG' in nextln else ''
		attrs += fg (self.colour)
		sys.stdout.write (attrs + '%s: %s' % (self.name, nextln) + attr (0))
		sys.stdout.flush ()
		self.sobad += nextln

	def run (self):
		self.copyloop ('')
		if self.phase1locker:
			phase1_mutex.acquire ()
			phase1_threads.remove (self)
			phase1_mutex.notify ()
			phase1_mutex.release ()

	def could_signal (self):
		return self.signals != []

	def send_signals (self):
		if self.proc.returncode is not None:
			return
		for (signame,signum) in self.signals:
			for pidf in self.pidfiles:
				pid = open (pidf).read ()
				os.kill (pid, signum)
			self.proc.send_signal (signum)
			# self.proc.wait (timeout=1.0)

	def terminate (self, signum=signal.SIGTERM):
		# if self.proc.returncode is not None:
		# 	return
		self.proc.send_signal (signum)

	def initial_result (self):
		# Result code or None
		self.join (0.0)
		if not self.isAlive ():
			return None
		self.proc.poll ()
		return self.proc.returncode

	def fetch_errors (self):
		return self.sobad

	def final_result (self):
		self.join ()
		self.ertap.join ()
		if self.proc.returncode is None:
			self.proc.wait ()
		return self.error or self.proc.returncode

	def final_judgement (self):
		exitval = self.final_result ()
		return (exitval, exitval in self.ok_exit)

	def final_output (self):
		self.join ()
		self.ertap.join ()
		if self.proc.returncode is None:
			self.proc.wait ()
		return self.sofar

strace = os.getenv ('STRACE')
valgrind = os.getenv ('VALGRIND')
syncprox = []
for (proc,pidl,zig,excitement,forkmark,name) in zip (prox,pidz,zigz,exok,fork,namz):
	if excitement == []:
		excitement = [ 0 ]
	if name is None:
		pni = 0
		if 'python' in proc [0]:
			pni = 1
		elif 'valgrind' in proc [0]:
			pni = 1
			while proc [pni] != '--' and proc [pni] [:1] == '-':
				pni += 1
		name = proc [pni].split ('/') [-1].split ('\\') [-1]
	else:
		if strace is not None:
			proc = ['strace', '-o', name + '.strace'] + strace.split () + ['--'] + proc
		if valgrind is not None:
			proc = ['valgrind', '--log-file=%s.valgrind' % name] + valgrind.split () + ['--'] + proc
	sys.stdout.write ('pypeline: initial: %s == %r\n' % (name,proc) + attr (0))
	newsyncer = SyncedProcess (proc, name=name, signals=zig, ok_exit=excitement, fork=forkmark, pidfiles=pidl, env=env)
	syncprox.insert (0, newsyncer)
	sys.stdout.write ('pypeline: freerun: %s\n' % (name,) + attr (0))
	newsyncer.start ()

#
# We enter the end game.  This consists of three phasis, during which
# the programs are graceffully terminated.  There is no overall timer,
# as each individual program is supposed to have a timeout or respond
# to a termination signal.
#
# The first pass waits until all programs without a registered signal
# have ended their actions.  An an alternative, it can also wait for
# signals SIGTERM or SIGHUP.  The second phase signals to the other
# programs (when they are still running) in an attempt to end them.
# The third phase finally waits until all programs which were signaled
# have also terminated.  This allows daemon-style programs to run as
# long as required, but terminate as soon the time-dependent work is
# complete.
#
# Actions are still made in the reverse order of appearance on the
# command line, but any order would work just as well.  The magic
# is in the ordering in phases, not the ordering on the command line.
#

def sighdl (sig, _st):
	global active, termination_signal
	sys.stdout.write ('pypeline: interrupt: signal %d will be sent as %d\n' % (sig,termination_signal))
	for sync in active:
		sys.stdout.write (' - termination signal %d to pid %d of %s\n' % (termination_signal,sync.proc.pid,sync.name))
		sync.terminate (termination_signal)
	termination_signal = signal.SIGKILL
	phase1_mutex.acquire ()
	phase1_mutex.notify ()
	phase1_mutex.release ()
	sys.stdout.write ('pypeline: interrupt: done\n')

termination_signal = signal.SIGTERM
signal.signal (signal.SIGHUP,  sighdl)
signal.signal (signal.SIGTERM, sighdl)

sys.stdout.flush ()
sys.stderr.flush ()
sys.stdout.write ('pypeline: end game, stage 1/3.  Await self-terminating programs.\n' + attr (0))
phase1_mutex.acquire ()
while len (phase1_threads) > 0:
	active = phase1_threads.copy ()
	phase1_mutex.wait ()
	for sync in active:
		if sync not in phase1_threads:
			sys.stdout.write ('pypeline: stopped: %s\n' % (sync.name,) + attr (0))
phase1_mutex.release ()

sys.stdout.flush ()
sys.stderr.flush ()
sys.stdout.write ('pypeline: end game, stage 2/3.  Sending registered termination signals.\n')
for sync in syncprox:
	sync.send_signals ()

sys.stdout.flush ()
sys.stderr.flush ()
sys.stdout.write ('pypeline: end game, stage 3/3.  Wait until signal-registering programs have ended.\n')
exitval = 0
for (sync,proc) in zip (syncprox,prox):
	(result,result_ok) = sync.final_judgement ()
	if result_ok:
		sys.stdout.write (('\n\npypeline: success: %s -> ' + attr ('bold') + 'exit (%d)' + attr (0) + '\n') % (sync.name, result))
	else:
		sys.stdout.write ((    'pypeline: failure: %s -> ' + attr ('bold') + 'exit (%d)' + attr (0) + '\n') % (sync.name, result))
		exitval = 1
	sys.stdout.write ('\n' + fg (sync.colour) + attr ('reverse') + attr ('bold') + (('FINAL ERRORS FROM %s ARE:' + attr (0) + '\n') % (sync.name.upper (),)))
	for ln in sync.fetch_errors ().split ('\n'):
		attrs = attr ('bold') if not 'DEBUG' in ln else ''
		attrs += fg (sync.colour)
		sys.stdout.write (attrs + ln + attr (0) + '\n')
	sys.stdout.write ('\n' + fg (sync.colour) + attr ('reverse') + (('FINAL OUTPUT FROM %s IS:' + attr (0) + '\n')  % (sync.name.upper (),)))
	for ln in sync.final_output ().split ('\n'):
		attrs = attr ('dim') if 'DEBUG' in ln else ''
		attrs += fg (sync.colour)
		sys.stdout.write (attrs + ln + attr (0) + '\n')
	if valgrind is not None and sync.name in namz:
		sys.stdout.write ('\n' + fg (sync.colour) + attr ('reverse') + (('VALGRIND ANALYSIS OF %s IS:' + attr (0) + '\n')  % (sync.name.upper (),)))
		for ln in open (sync.name + '.valgrind').readlines ():
			ln = ln.rstrip ()
			attrs = attr ('dim') if False else ''
			attrs += fg (sync.colour)
			sys.stdout.write (attrs + ln + attr (0) + '\n')
	if strace is not None and sync.name in namz:
		sys.stdout.write ('\n' + fg (sync.colour) + attr ('reverse') + (('STRACE ANALYSIS OF %s IS:' + attr (0) + '\n')  % (sync.name.upper (),)))
		sticky_attrs = attr ('dim')
		for ln in open (sync.name + '.strace').readlines ():
			ln = ln.rstrip ()
			if ln [:6] == 'rt_':
				sticky_attrs = ''
			attrs = sticky_attrs + fg (sync.colour)
			sys.stdout.write (attrs + ln + attr (0) + '\n')

sys.stdout.flush ()
sys.stderr.flush ()
for (argi,portnum) in port_alloc.items ():
	proto = argi [:1+argi.find (':')]
	sox = socket.socket ( *soxtypes [proto] )
	try:
		sox.bind ( (myip, portnum) )
	except:
		sys.stderr.write ('pypeline: cleanup: Socket %s was not properly closed\n' % (argi))
		# exitval = 1
	finally:
		sox.close ()

if tmpdir is not None:
	for tmpfn in os.listdir (tmpdir):
		os.remove (tmpdir + os.sep + tmpfn)
	os.rmdir (tmpdir)

sys.exit (exitval)

