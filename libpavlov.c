/* libpavlov -- expect/response interaction to replace chat(8)
 *
 * The program now lives on its own in
 * https://gitlab.com/arpa2/pavlov
 *
 * We use a few expect/response notations in the TLS Pool, and relied on
 * chat(8) which did such things for pppd.  This was less than perfect.  We now
 * change that to libpavlov, with a straightforward extension to a main program,
 * that allows us to play interactive scripts with some degree of flexibility.
 *
 * This is used in:
 *  - tlstunnel                  --> to initiate a STARTTLS sequence
 *  - testcli, testsrv, testpeer --> to play standardised interactions over TLS
 *
 * The general idea for the command set is documented in an issue:
 * https://github.com/arpa2/tlspool/issues/110
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <unistd.h>
#include <errno.h>
#include <regex.h>
#include <sys/types.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <com_err/pavlov.h>



/** @brief Pavlov calling point for stimulus-response interaction.
 *
 * This is the calling point for Pavlov interaction.  It runs
 * over the commands in argv[0] through argv[argc-1] while
 * reading from fdin and writing to fdout.
 *
 * The commands each express on of the following strings:
 *
 *   - `!<data>` --> send out the data
 *   - `?<regex>` --> receive >0 bytes and recognise the POSIX extended regex
 *   - `@<msdelay>` --> delay in milli-seconds
 *   - `*<regex>` --> skip messages preceding a `?` match
 *
 * The additional command `*<regex>` can occur any number of times before a
 * `?<regex>` command, and any sequence of any number of any of these are
 * skipped before the `?<command>` is tried.  Pavlov no longer silently drops
 * unrecognised messages; this behaviour causes locked test chains, and more
 * elaborate error reporting was therefore called for.  You can still specify
 * catch-all patterns, but need to do so explicitly.
 *
 * @param[in] fdin     file descriptor for interactive input
 * @param[in] fdout    file descriptor for interactive output
 * @param[in] progname program name, like argv[0] in main
 * @param[in] argc     argument count after program name
 * @param[in] argv     argument vector after program name
 *
 * @returns            0 for a successful end, or the argument
 *                     number plus one for an error.  In case
 *                     of an error, errno is set to a POSIX error
 *                     or one from the Pavlov error table.  To
 *                     benefit from the latter, use com_err(3)
 *                     after calling initialize_PAVL_error_table().
 */
int pavlov (int fdin, int fdout,
		char *progname, int argc, char *argv[]) {
	//
	// Initialize the error table (just once)
	static bool have_et = false;
	if (!have_et) {
		initialize_PAVL_error_table ();
		have_et = true;
	}
	//
	// Iterate through arguments, starting at argv [0]
	char databuf [4096 + 1];
	int argi = 0;
	while (argi < argc) {
		char *arg = argv [argi];
		if (arg == NULL) {
			errno = PAVLOV_ILLEGAL_STRUCTURE;
			goto fail;
		}
		log_debug ("%s: Pavlov command %d/%d: %s", progname, 1+argi, argc, arg);
		char cmd = *arg++;
		switch (cmd) {
		//
		// Command "handler" for no command at all
		case '\0':
			errno = PAVLOV_ILLEGAL_STRUCTURE;
			goto fail;
		//
		// Command handler for writing output
		case '!':
			write (fdout, arg, strlen (arg));
			break;
		//
		// Command handler for '*' skips and leaves the work to following '?'
		case '*':
			break;
		//
		// Command handler for extended regular extensions
		case '?':
			{ }
			int datalen = read (fdin, databuf, sizeof (databuf) - 1);
			if (datalen < 0) {
				/* errno is set -- and probably quite informative */
				goto fail;
			} else if (datalen == 0) {
				errno = PAVLOV_INPUT_BROKEN;
				goto fail;
			}
			if (memchr (databuf, '\0', datalen) != NULL) {
				errno = PAVLOV_INPUT_BINARY;
				goto fail;
			}
			databuf [datalen] = '\0';
			log_detail ("%s: Pavlov input message %s", progname, databuf);
			int argj = argi;
			bool notfound = true;
			do {
				regex_t rexj;
				if (regcomp (&rexj, argv [argj] + 1, REG_EXTENDED | REG_NEWLINE | REG_NOSUB) != 0) {
					/* errno is set */
					goto fail;
				}
				notfound = (regexec (&rexj, databuf, 0, NULL, 0) != 0);
				regfree (&rexj);
			} while (notfound && (argj-- > 0) && (*argv [argj] == '*'));
			if (notfound) {
				errno = PAVLOV_INPUT_NOMATCH;
				goto fail;
			}
			break;
		//
		// Command handler for microsecond delays
		case '@':
			{ }
			long msdelay = strtol (arg, &arg, 10);
			if (*arg != '\0') {
				errno = PAVLOV_TIMEOUT_SYNTAX;
				goto fail;
			}
			useconds_t usleep_delay = msdelay * 1000;
			if (usleep_delay / 1000 != msdelay) {
				errno = PAVLOV_TIMEOUT_SYNTAX;
				goto fail;
			}
			usleep (usleep_delay);
			break;
		//
		// Default handling for unknown commands
		default:
			errno = PAVLOV_UNRECOGNISED_COMMAND;
			goto fail;
		//
		// End of command switch
		}
		argi++;
	}
	//
	// Done.  Return success.
	log_debug ("%s: Pavlov succeeded", progname);
	return 0;
	//
	// Return in turmoil
fail:
	com_err (progname, errno, ": Pavlov failed on %s", argv [argi]);
	return 1 + argi;
}

