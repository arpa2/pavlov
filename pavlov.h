/** @defgroup arpa2pavlov Pavlov API
 * @{
 */

/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */

/** @brief Pavlov calling point for stimulus-response interaction.
 *
 * This is the calling point for Pavlov interaction.  It runs
 * over the commands in argv[0] through argv[argc-1] while
 * reading from fdin and writing to fdout.
 *
 * The commands each express on of the following strings:
 *
 *   - `!<data>` --> send out the data
 *   - `?<regex>` --> receive >0 bytes and recognise the POSIX extended regex
 *   - `@<msdelay>` --> delay in milli-seconds
 *
 * @param[in] fdin     file descriptor for interactive input
 * @param[in] fdout    file descriptor for interactive output
 * @param[in] progname program name, like argv[0] in main
 * @param[in] argc     argument count after program name
 * @param[in] argv     argument vector after program name
 *
 * @returns            0 for a successful end, or the argument
 *                     number plus one for an error.  In case
 *                     of an error, errno is set to a POSIX error
 *                     or one from the Pavlov error table.  To
 *                     benefit from the latter, use com_err(3)
 *                     after calling initialize_PAVL_error_table().
 */
int pavlov (int fdin, int fdout,
		char *progname, int argc, char *argv[]);

/** @} */
